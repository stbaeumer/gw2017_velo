**Situation: Artikelverwaltung Velo 2000**

Die Velo2000 GmbH produziert individuell nach Kundenwünschen gefertigte
Fahrräder für den gehobenen Bedarf. Zusätzlich handelt Velo2000 mit
Fahrrädern aus den Produktgrup­pen Mountainbike, Rennrad und Faltrad,
die von unterschiedlichen Lieferanten bezogen werden. Das
Handels-Sortiment besteht aus den unten abgebildeten Artikeln. Velo 2000
kalkuliert den Verkaufspreis der Produktgruppe Mountainbike mit einem
Zuschlagsatz von 60 %, bei Rennrädern mit 70 % und bei Falträdern mit
75 %. Entwickeln Sie eine Anwendung, mit deren Hilfe die Artikel der
Velo2000 GmbH praxisgerecht verwaltet werden können.
  
  **- Artikelliste –**

 |**Produktgruppe**|**ArtikelNr**|**Artikelname**|**EK-Preis**|**VK-Preis**|
 |---|---|---|---|---|
 |F1: MOUNTAINBIKE|110|Fun|185 €|296 €|
 |F1: MOUNTAINBIKE|120|Solid|265 €|424 €|
 |F1: MOUNTAINBIKE|130|Experience|620 €|992 €|
 |F2: RENNRAD|210|Fortune|270 €|459 €|
 |F2: RENNRAD|220|Ambition|350 €|595 €|
 |F2: RENNRAD|230|Racemaker|580 €|986 €|
 |F3: FALTRAD|310|Birdy|240 €|420 €|
 |F3: FALTRAD|320|Tiny|420 €|735 €|
 |F3: FALTRAD|330|Peasy|480 €|840 €|


**Aufgabe 1: **

Erstellen Sie ein neues Projekt zur Verwaltung von Fahrrädern bei der
Velo2000 in Anlehnung an das Kontoprojekt.

**Aufgabe 2: **
Erweitern Sie das Projekt zur Verwaltung
von Fahrrädern bei der Velo2000 Beim Laden des Formulars sollen die Objekte Fun und Solid deklariert, instanziiert und initialisiert werden. Beim Klick auf „neues Fahrrad anlegen“ soll MeinFahrrad instanziiert und initialisiert werden. 

**Aufgabe 3: **

Erweitern Sie das Projekt zur Verwaltung von Fahrrädern bei der Velo2000. Für jedes Fahrrad soll die Marge (Differenz zwischen VK und EK) in
    Euro und in Prozent ausgegeben werden. Entsprechend muss eine neue
    Methode erstellt werden (siehe Klassendiagramm) und die Methode muss
    für jedes Objekt aufgerufen werden.


Für MeinFahrrad soll der VK erhöht und gesenkt werden können. 

**Aufgabe 4: **
